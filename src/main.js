import Vue from "vue";
import VueNativeSock from "vue-native-websocket";
import settings from "electron-settings";
import axios from "axios";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import { remote, webFrame } from "electron";
import { Heartbeat, Modes } from "@dimensional-innovations/node-heartbeat";

const config = settings.getSync("config");

Vue.http = Vue.prototype.$http = axios;
Vue.config.productionTip = false;

Vue.use(
  VueNativeSock,
  `ws://${config.websocket.host}:${config.websocket.port}`,
  {
    format: "json",
    store,
    reconnection: true
  }
);

new Vue({
  router,
  store,
  render: function(h) {
    return h(App);
  }
}).$mount("#app");

webFrame.setVisualZoomLevelLimits(1, 1);

document.addEventListener("contextmenu", e => {
  e.preventDefault();
  return false;
});

new Heartbeat({
  apiKey: null, // TODO: go create a new hearbeat token at https://betteruptime.com/team/8248/heartbeats
  enabled: remote.app.isPackaged,
  mode: Modes.BROWSER,
}).start();

if (remote.app.isPackaged) {
  document.getElementsByTagName('html')[0].classList.add('is-packaged');
  remote.getCurrentWindow().setSize(
    settings.getSync("config.appWidth"),
    settings.getSync("config.appHeight"),
  );
}