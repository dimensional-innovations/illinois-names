import Vue from "vue";
import Vuex from "vuex";
//import { setupCache } from "axios-cache-adapter";
//import Axios from "axios";

Vue.use(Vuex);

// const cache = setupCache({
//   maxAge: 60 * 60 * 1000,
//   exclude: { query: false }
// });

//const axios = Axios.create({ adapter: cache.adapter });

const api = {
  state: {
    loading: true,
    awardIndex: 0,
    awardTypes: [{name: "letterwinners", key: "letter_winners"}, {name: "all-americans", key: "all_americans"}, {name: "all b1g", key: "all_b1g"}],
    activeAwardType: {name: "letterwinners", key: "letter_winners"},
    activeLetterWinnerDecade: null,
    activeSeason: null,
    activePlayer: null,
    players: null,
    recruitingVideos: [],
    fullListPlayers: []
  },
  mutations: {
    setActivePlayer(state, id) {
      if (id) state.activePlayer = state.fullListPlayers.find(player => player.player.id === parseInt(id));
      else state.activePlayer = null;
    },
    setActiveSeason: (state, data) => (state.activeSeason = data),
    setActiveAward: (state, data) => {
      state.activeAwardType = state.awardTypes.find(award => award.key === data);
    },
    setActiveLetterWinnerDecade: (state, data) => (state.activeLetterWinnerDecade = data),
    set_awards: (state, data) => {
      state.players = data;
      state.activeLetterWinnerDecade = Object.keys(data[state.activeAwardType.key])[0];
    },
    set_recruitingvideos: (state, data) => (state.recruitingVideos = data),
    setFullListPlayers: (state, data) => (state.fullListPlayers = data),
    setLoading: (state, data) => (state.loading = data)
  },
  actions: {}
}

export default api;