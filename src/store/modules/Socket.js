import Vue from "vue";

const state = {
  isConnected: false,
  reconnectError: false
};

const mutations = {
  SOCKET_ONOPEN(state, event) {
    Vue.prototype.$socket = event.currentTarget;
    state.isConnected = true;
  },
  SOCKET_ONCLOSE(state, event) {
    state.isConnected = false;
  },
  SOCKET_ONERROR(state, event) {
    console.error(state, event);
  },
  SOCKET_ONMESSAGE(state, message) {
    //console.log("SOCKET_ONMESSAGE", message);
  },
  SOCKET_RECONNECT(state, count) {
    console.info(state, count);
  },
  SOCKET_RECONNECT_ERROR(state) {
    state.reconnectError = true;
  }
};

const actions = {
  sendMessage: function(context, message) {
    Vue.prototype.$socket.send(message);
  }
};

export default {
  state,
  mutations,
  actions
};
