import { init } from '@dimensional-innovations/vue-electron-background';
import { app } from 'electron';
import { config } from '../package';
import settings from "electron-settings";

const { isPackaged } = app;

init({
  enableKioskMode: isPackaged,
  enableAutoUpdater: isPackaged,
  config,
  browserWindowOptionOverrides: {
    x: 0,
    y: 0,
    fullscreen: false,
    kiosk: false,
    frame: !isPackaged,
    minWidth: isPackaged ? settings.getSync("config.appWidth") : 960,
    minHeight: isPackaged ? settings.getSync("config.appHeight") : 540,
  }
});
