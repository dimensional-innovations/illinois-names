const { name } = require('./package.json');

const publishConfig = {
  provider: 's3',
  bucket: 'di-ci-cd',
  acl: 'public-read',
  region: 'us-east-2',
  path: `/${name}/release`,
};

module.exports = {
  pluginOptions: {
    electronBuilder: {
      nodeIntegration: true,
      builderOptions: {
        generateUpdatesFilesForAllChannels: true,
        publish: [
          {
            ...publishConfig,
            channel: 'latest',
          },
          {
            ...publishConfig,
            channel: 'alpha',
          },
        ],
      },
    },
  },
  css: {
    loaderOptions: {
      sass: {
        prependData: "@import '@/assets/scss/_includes.scss';",
      },
    },
  },
};
