# DI Vue

A Vue/Electron boilerplate.

## Getting Started

1. Follow the guide on [setting up gitlab packages](https://gitlab.com/dimensional-innovations/di-handbook/-/blob/master/gitlab-packages/gitlab-packages-setup.md). This will ensure you're authenticated with Dimensional Innovation's private package repository hosted on Gitlab.
2. Fork this repository. [How to fork a project](https://docs.gitlab.com/ee/gitlab-basics/fork-project.html).
3. Clone your forked repository.
4. **Change the name value in the package.json file to a unique name for your project**. This is *VERY* important. The name field controls the location of deployments (an S3 bucket) and consequently the auto updating (see the deployments section below) rules for an app. Apps with matching names will overwrite the deployment location and will unexpectedly update.
5. Run `yarn install` to install all dependencies.
6. Run `yarn dev` to start a local server and launch the app.


## What's Included?

* [@dimensional-innovations/electron-logger](https://gitlab.com/dimensional-innovations/electron-logger): an improved logger which writes all your logs to the desktop, the browser console, terminal and [Papertrail](https://www.papertrail.com/).
* [@dimensional-innovations/node-heartbeat](https://gitlab.com/dimensional-innovations/node-heartbeat): broadcast heartbeat messages to [BetterUptime](https://dimin.betteruptime.com/) for alerting and monitoring.
* [@dimensional-innovations/vue-electron-background](https://gitlab.com/dimensional-innovations/vue-electron-background): enables kiosk mode and auto updating
* [@dimensional-innovations/vue-electron-idle](https://gitlab.com/dimensional-innovations/vue-electron-idle): notify when an app has become idle
* [@dimensional-innovations/vue-electron-version](https://gitlab.com/dimensional-innovations/vue-electron-version): briefly display the application version for debugging
* [electron-settings](https://github.com/nathanbuchar/electron-settings#readme): settings management for electron apps
* [normalize.css](https://necolas.github.io/normalize.css/): make all browsers render elements consistently
* [vue 2.x](https://vuejs.org/): Vue
* [vue router](https://router.vuejs.org/): A router for Vue
* [vuex](https://vuex.vuejs.org/): State management for Vue
* [vue cli](https://cli.vuejs.org/): A CLI for common Vue tasks
* [electron](https://www.electronjs.org/): Make web apps downloadable as a desktop app

## Deployments

This project implements [electron's autoUpdater concept](https://www.electronjs.org/docs/api/auto-updater) via the shared [@dimensional-innovations/vue-electron-background](https://gitlab.com/dimensional-innovations/vue-electron-background) package. After the initial install of an app on a host machine, the application will automatically update itself everytime a new release is created.

To deploy:

1. Click "releases" under "Project overview" on your project's gitlab page.
2. Create a "new release".
3. Choose a [semantic version name](https://semver.org/). If you're not sure what to put, use `v0.0.1` and increment (ex. `v0.0.2`) everytime you create a new release.
4. Give the release a good title and a thorough description. Click "create release".
5. Click on the "CI / CD" tab of your project's Gitlab interface and navigate to "Pipelines". You should see your application building. This will take 5 or so minutes. If all steps were succesful, you'll see one or more green checkmarks and your app will have sucessfully built.
6. On this same pipeline screen, you'll be able to download "release artifacts". These artifacts contain the executable file to install on your host machine.
7. Once installed, you can follow steps 1 through 4 anytime you'd like to create new releases (usually after code is merged to the master branch). 

## FAQ
### Why node v14.17.0? 
This version is enforced because we use the [electronuserland/builder:wine](https://hub.docker.com/r/electronuserland/builder/) docker container to build and deploy applications. This container should eventually be updated and republished.